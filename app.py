from flask import Flask, render_template, request


DEBUG = True or False  # escolha um

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')


# pegando informação da url (/terms/qualquer_coisa/, por exemplo)
@app.route('/terms/')
@app.route('/terms/<variavel>/')
def terms(variavel=None):
    return render_template('terms.html', nome_variavel=variavel)


# pegando informação GET (/profile/?id=3, por exemplo)
@app.route('/profile/')
def profile():
    # pra acessar parâmetros GET, usa-se request.args
    # pra acessar parâmetros POST, usa-se request.form
    # pra acessar arquivos enviados por POST, request.files

    profile_id = None
    if 'id' in request.args:
        profile_id = request.args['id']

    return render_template('profile.html', id=profile_id)


if __name__ == '__main__':
    app.run(debug=DEBUG, port=8080)
